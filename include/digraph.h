#ifndef DIGRAPH_H
#define DIGRAPH_H

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <stack>

class Node {
public:
    // properties (AKA data members, fields, attributes)
    std::string payload; // data contents of the node

    // methods (AKA member functions)
    // Node::toString(): returns the payload as a string (which, in this case, it already is)
    std::string toString() const;
    bool operator==(const Node& other) const;

    // constructors
    Node() = default; // default constructor
    // Node::Node(std::string payload_in): sets the "payload" property equal to payload_in
    Node(std::string payload_in); 
};

// we use this struct to keep track of how we traverse the graph
struct NodeVisitor {
    // properties
    std::shared_ptr<NodeVisitor> parent;
    std::shared_ptr<Node> node;
    int cost;
};

class DirectedEdge {
public:
    // properties
    std::shared_ptr<Node> source; // shared_ptr to the start node of the edge
    std::shared_ptr<Node> sink; // shared_ptr to the end node of the edge
    int cost; // cost of traversing the directed edge

    // methods
    // DirectedEdge::toString(): returns string in the form "source -- cost -- sink"
    std::string toString() const;

    // constructors
    DirectedEdge() = default; // default constructor
    /*
        DirectedEdge::DirectedEdge(std::shared_ptr<Node> source_in, 
                                    std::shared_ptr<Node> sink_in, int cost_in): 
            sets the source property equal to source_in, the sink property equal
            to sink_in, and the cost property equal to cost_in.
    */
    DirectedEdge(std::shared_ptr<Node> source_in, std::shared_ptr<Node> sink_in, int cost_in);
};

class Digraph {
public:
    // properties
    std::vector<std::shared_ptr<DirectedEdge> > edges;

    // methods
    /*
        Digraph::Neighbors(const std::shared_ptr<Node> node): 
            eturns a vector of shared pointers to nodes containing the immediate neighbors of the argument node.
    */
    std::vector<std::shared_ptr<Node> > Neighbors(const std::shared_ptr<Node> node) const;
    // Digraph::toString(): Returns string representations of edges, separated by newlines.
    std::string toString() const;

    Digraph() = default; // default constructor
    /*
        Digraph::Digraph(std::vector<std::shared_ptr<DirectedEdge> > edges_in):
            sets edges equal to edges_in.
    */
    Digraph(std::vector<std::shared_ptr<DirectedEdge> > edges_in);
};

#endif