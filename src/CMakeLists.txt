include_directories(${digraph_SOURCE_DIR}/include)

add_executable(testing
	test.cc
	digraph.cc
)

add_custom_command(
    TARGET testing POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/testing${CMAKE_EXECUTABLE_SUFFIX}" "${digraph_SOURCE_DIR}/bin/"
)
